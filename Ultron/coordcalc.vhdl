library IEEE;
use IEEE.std_logic_1164.ALL;
use ieee.numeric_std.all;

architecture behaviour of coordcalc is

signal x_0: std_logic_vector(6 downto 0) := "0110110"; --std_logic_vector(54);	-- 60 - 4
signal y_0: std_logic_vector(7 downto 0) := "10000010"; --std_logic_vector(130);	-- 134 - 4
signal sinarray, sinarrayY: std_logic_vector(5 downto 0);

begin

P1: process(clk, reset)
begin

if clk'event and clk = '1' then
	if (reset = '1') then
		x_red <= "0000000";
		y_red <= "00000000";
		x_blue <= "0000000";
		y_blue <= "00000000";
		sinarray <= "000000";
		sinarrayY <= "000000";
	else
	case step is
		when "0000" =>
			sinarray <= "010011";
			sinarrayY <= "000000";
		when "0001" =>
			sinarray <= "010011";
			sinarrayY <= "000100";
		when "0010" =>
			sinarray <= "010010";
			sinarrayY <= "000111";
		when "0011" =>
			sinarray <= "010000";
			sinarrayY <= "001011";
		when "0100" =>
			sinarray <= "001101";
			sinarrayY <= "001101";
		when "0101" =>
			sinarray <= "001011";
			sinarrayY <= "010000";
		when "0110" =>
			sinarray <= "000111";
			sinarrayY <= "010010";
		when "0111" =>
			sinarray <= "000100";
			sinarrayY <= "010011";
		when "1000" =>
			sinarray <= "000000";
			sinarrayY <= "010011";
		when others =>
			sinarray <= "000000";
			sinarrayY <= "000000";
	end case;

	case quadrant(0) is
		when '0' => 		x_red <= std_logic_vector(unsigned(x_0) - unsigned(sinarray));
				x_blue <= std_logic_vector(unsigned(x_0) + unsigned(sinarray));
		when '1' => 		x_red <= std_logic_vector(unsigned(x_0) + unsigned(sinarray));
				x_blue <= std_logic_vector(unsigned(x_0) - unsigned(sinarray));
		when others => x_red <= "0000000"; x_blue <= "0000000";
	end case;
	case quadrant(1) is
		when '0' =>	 	y_red <= std_logic_vector(unsigned(y_0) - unsigned(sinarrayY));
			     	y_blue <= std_logic_vector(unsigned(y_0) + unsigned(sinarrayY));
		when '1' =>		y_red <= std_logic_vector(unsigned(y_0) + unsigned(sinarrayY));
			     	y_blue <= std_logic_vector(unsigned(y_0) - unsigned(sinarrayY));
		when others => y_red <= "00000000"; y_blue <= "00000000";
	end case;
	end if;
end if;

end process;

end behaviour;
