library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.STD_LOGIC_UNSIGNED.all;

entity vga_startscherm is
	PORT(clk, reset  :in 	std_logic;
	rom_data	 :in    std_logic_vector(7 downto 0);
	rom_address	 :out	std_logic_vector(5 downto 0);
	row_address      :in 	std_logic_vector(6 downto 0);
    col_address 	 :in 	std_logic_vector(7 downto 0);   
    rom_mux_output   :out   std_logic);
end entity;