
library ieee,CellsLib;

use ieee.std_logic_1164.all;
use CellsLib.CellsLib_DECL_PACK.all;

architecture synthesised of bar is

   component iv110
      port( A : in std_logic;  Y : out std_logic);
   end component;
   
   component na210
      port( A, B : in std_logic;  Y : out std_logic);
   end component;
   
   component no210
      port( A, B : in std_logic;  Y : out std_logic);
   end component;
   
   component ex210
      port( A, B : in std_logic;  Y : out std_logic);
   end component;
   
   component na310
      port( A, B, C : in std_logic;  Y : out std_logic);
   end component;
   
   component no310
      port( A, B, C : in std_logic;  Y : out std_logic);
   end component;
   
   component mu111
      port( A, B, S : in std_logic;  Y : out std_logic);
   end component;
   
   component dfa11
      port( D, CK, R : in std_logic;  Q : out std_logic);
   end component;
   
   signal output_y0_7_port, output_y0_6_port, output_y0_5_port, 
      output_y0_4_port, output_y0_3_port, output_y0_2_port, output_y0_1_port, 
      output_y0_0_port, output_type0_2_port, output_type0_1_port, 
      output_type0_0_port, output_type1_2_port, output_type1_1_port, 
      output_type1_0_port, output_type2_2_port, output_type2_1_port, 
      output_type2_0_port, output_type3_2_port, output_type3_1_port, 
      output_type3_0_port, N181, N182, N183, N184, N185, N186, N187, N188, N189
      , N190, N191, N192, N193, N194, N195, N196, N197, N198, N199, N200, N201,
      N202, N203, N204, N205, N206, N207, N208, N210, N211, N212, N213, N214, 
      N215, N217, N218, N219, N220, N221, n101, n102, n103, n104, n105, n106, 
      n107, n108, n109, n110, n111, n112, n113, n114, n115, n116, n117, n118, 
      n119, n120, n121, n122, n123, n124, n125, n126, n127, n128, n129, n130, 
      n131, n132, n133, n134, n135, n136, n137, n138, n139, n140, n141, n142, 
      n143, n144, n145, n146, n147, n148, n149, n150, n151, n152, n153, n154, 
      n155, n156, n157, n158, n159, n160, n161, n162, n163, n164, n165, n166, 
      n167, n168, n169, n170, n171, n172, n173, n174, n175, n176, n177, n178, 
      n179, n180, n181_port, n182_port, n183_port, n184_port, n185_port, 
      n186_port, n187_port, n188_port, n189_port, n190_port, n191_port, 
      n192_port, n193_port, n194_port, n195_port, n196_port, n197_port, 
      n198_port, n199_port : std_logic;

begin
   output_y0 <= ( output_y0_7_port, output_y0_6_port, output_y0_5_port, 
      output_y0_4_port, output_y0_3_port, output_y0_2_port, output_y0_1_port, 
      output_y0_0_port );
   output_type0 <= ( output_type0_2_port, output_type0_1_port, 
      output_type0_0_port );
   output_type1 <= ( output_type1_2_port, output_type1_1_port, 
      output_type1_0_port );
   output_type2 <= ( output_type2_2_port, output_type2_1_port, 
      output_type2_0_port );
   output_type3 <= ( output_type3_2_port, output_type3_1_port, 
      output_type3_0_port );
   
   bar_y0_reg_0_inst : dfa11 port map( D => n197_port, CK => counter, R => 
                           reset, Q => output_y0_0_port);
   bar_y0_reg_5_inst : dfa11 port map( D => N183, CK => counter, R => reset, Q 
                           => output_y0_5_port);
   bar_y0_reg_6_inst : dfa11 port map( D => N184, CK => counter, R => reset, Q 
                           => output_y0_6_port);
   bar_y0_reg_4_inst : dfa11 port map( D => N182, CK => counter, R => reset, Q 
                           => output_y0_4_port);
   bar_y0_reg_3_inst : dfa11 port map( D => N181, CK => counter, R => reset, Q 
                           => output_y0_3_port);
   bar_y0_reg_2_inst : dfa11 port map( D => n199_port, CK => counter, R => 
                           reset, Q => output_y0_2_port);
   bar_y0_reg_1_inst : dfa11 port map( D => n198_port, CK => counter, R => 
                           reset, Q => output_y0_1_port);
   bar_type3_reg_0_inst : dfa11 port map( D => N219, CK => counter, R => reset,
                           Q => output_type3_0_port);
   bar_type3_reg_1_inst : dfa11 port map( D => N220, CK => counter, R => reset,
                           Q => output_type3_1_port);
   bar_type3_reg_2_inst : dfa11 port map( D => N221, CK => counter, R => reset,
                           Q => output_type3_2_port);
   bar_y0_reg_7_inst : dfa11 port map( D => N185, CK => counter, R => reset, Q 
                           => output_y0_7_port);
   bar_y1_reg_7_inst : dfa11 port map( D => N193, CK => counter, R => reset, Q 
                           => output_y1(7));
   bar_y1_reg_6_inst : dfa11 port map( D => N192, CK => counter, R => reset, Q 
                           => output_y1(6));
   bar_y1_reg_5_inst : dfa11 port map( D => N191, CK => counter, R => reset, Q 
                           => output_y1(5));
   bar_y1_reg_4_inst : dfa11 port map( D => N190, CK => counter, R => reset, Q 
                           => output_y1(4));
   bar_y1_reg_3_inst : dfa11 port map( D => N189, CK => counter, R => reset, Q 
                           => output_y1(3));
   bar_y1_reg_2_inst : dfa11 port map( D => N188, CK => counter, R => reset, Q 
                           => output_y1(2));
   bar_y1_reg_1_inst : dfa11 port map( D => N187, CK => counter, R => reset, Q 
                           => output_y1(1));
   bar_y1_reg_0_inst : dfa11 port map( D => N186, CK => counter, R => reset, Q 
                           => output_y1(0));
   bar_y2_reg_7_inst : dfa11 port map( D => N201, CK => counter, R => reset, Q 
                           => output_y2(7));
   bar_y2_reg_6_inst : dfa11 port map( D => N200, CK => counter, R => reset, Q 
                           => output_y2(6));
   bar_y2_reg_5_inst : dfa11 port map( D => N199, CK => counter, R => reset, Q 
                           => output_y2(5));
   bar_y2_reg_4_inst : dfa11 port map( D => N198, CK => counter, R => reset, Q 
                           => output_y2(4));
   bar_y2_reg_3_inst : dfa11 port map( D => N197, CK => counter, R => reset, Q 
                           => output_y2(3));
   bar_y2_reg_2_inst : dfa11 port map( D => N196, CK => counter, R => reset, Q 
                           => output_y2(2));
   bar_y2_reg_1_inst : dfa11 port map( D => N195, CK => counter, R => reset, Q 
                           => output_y2(1));
   bar_y2_reg_0_inst : dfa11 port map( D => N194, CK => counter, R => reset, Q 
                           => output_y2(0));
   bar_y3_reg_6_inst : dfa11 port map( D => N208, CK => counter, R => reset, Q 
                           => output_y3(6));
   bar_y3_reg_5_inst : dfa11 port map( D => N207, CK => counter, R => reset, Q 
                           => output_y3(5));
   bar_y3_reg_4_inst : dfa11 port map( D => N206, CK => counter, R => reset, Q 
                           => output_y3(4));
   bar_y3_reg_3_inst : dfa11 port map( D => N205, CK => counter, R => reset, Q 
                           => output_y3(3));
   bar_y3_reg_2_inst : dfa11 port map( D => N204, CK => counter, R => reset, Q 
                           => output_y3(2));
   bar_y3_reg_1_inst : dfa11 port map( D => N203, CK => counter, R => reset, Q 
                           => output_y3(1));
   bar_y3_reg_0_inst : dfa11 port map( D => N202, CK => counter, R => reset, Q 
                           => output_y3(0));
   bar_type2_reg_2_inst : dfa11 port map( D => N218, CK => counter, R => reset,
                           Q => output_type2_2_port);
   bar_type1_reg_2_inst : dfa11 port map( D => N215, CK => counter, R => reset,
                           Q => output_type1_2_port);
   bar_type0_reg_2_inst : dfa11 port map( D => N212, CK => counter, R => reset,
                           Q => output_type0_2_port);
   bar_type2_reg_1_inst : dfa11 port map( D => N217, CK => counter, R => reset,
                           Q => output_type2_1_port);
   bar_type1_reg_1_inst : dfa11 port map( D => N214, CK => counter, R => reset,
                           Q => output_type1_1_port);
   bar_type0_reg_1_inst : dfa11 port map( D => N211, CK => counter, R => reset,
                           Q => output_type0_1_port);
   bar_type2_reg_0_inst : dfa11 port map( D => n196_port, CK => counter, R => 
                           reset, Q => output_type2_0_port);
   bar_type1_reg_0_inst : dfa11 port map( D => N213, CK => counter, R => reset,
                           Q => output_type1_0_port);
   bar_type0_reg_0_inst : dfa11 port map( D => N210, CK => counter, R => reset,
                           Q => output_type0_0_port);
   U130 : iv110 port map( A => n134, Y => n101);
   output_y3(7) <= '0';
   U132 : na310 port map( A => n102, B => n103, C => n104, Y => n196_port);
   U133 : na210 port map( A => output_type3_0_port, B => n101, Y => n104);
   U134 : na210 port map( A => output_type2_0_port, B => n106, Y => n103);
   U135 : na210 port map( A => n107, B => n108, Y => n102);
   U136 : no210 port map( A => output_y0_0_port, B => n101, Y => n197_port);
   U137 : no210 port map( A => n101, B => n109, Y => n198_port);
   U138 : no210 port map( A => n105, B => n110, Y => n199_port);
   U139 : na310 port map( A => n111, B => n112, C => n113, Y => N221);
   U140 : na210 port map( A => blocktype(2), B => n105, Y => n113);
   U141 : na210 port map( A => output_type3_2_port, B => n106, Y => n112);
   U142 : na310 port map( A => n111, B => n114, C => n115, Y => N220);
   U143 : na210 port map( A => blocktype(1), B => n105, Y => n115);
   U144 : na210 port map( A => output_type3_1_port, B => n106, Y => n114);
   U145 : na210 port map( A => n116, B => n107, Y => n111);
   U146 : iv110 port map( A => n117, Y => n116);
   U147 : na210 port map( A => n118, B => n119, Y => N219);
   U148 : na210 port map( A => output_type3_0_port, B => n106, Y => n119);
   U149 : na210 port map( A => blocktype(0), B => n105, Y => n118);
   U150 : na210 port map( A => n120, B => n121, Y => N218);
   U151 : na210 port map( A => output_type2_2_port, B => n106, Y => n121);
   U152 : na210 port map( A => output_type3_2_port, B => n105, Y => n120);
   U153 : na210 port map( A => n122, B => n123, Y => N217);
   U154 : na210 port map( A => output_type2_1_port, B => n106, Y => n123);
   U155 : na210 port map( A => output_type3_1_port, B => n105, Y => n122);
   U156 : na210 port map( A => n124, B => n125, Y => N215);
   U157 : na210 port map( A => output_type1_2_port, B => n106, Y => n125);
   U158 : na210 port map( A => output_type2_2_port, B => n105, Y => n124);
   U159 : na310 port map( A => n126, B => n127, C => n128, Y => N214);
   U160 : na210 port map( A => output_type2_1_port, B => n105, Y => n128);
   U161 : na210 port map( A => output_type1_1_port, B => n106, Y => n127);
   U162 : na210 port map( A => n129, B => n107, Y => n126);
   U163 : na210 port map( A => n130, B => n131, Y => N213);
   U164 : na210 port map( A => output_type1_0_port, B => n106, Y => n131);
   U165 : na210 port map( A => n105, B => output_type2_0_port, Y => n130);
   U166 : iv110 port map( A => n132, Y => N212);
   U167 : no210 port map( A => n133, B => n107, Y => n132);
   U168 : mu111 port map( A => output_type1_2_port, B => output_type0_2_port, S
                           => n134, Y => n133);
   U169 : na210 port map( A => n135, B => n136, Y => N211);
   U170 : na210 port map( A => output_type0_1_port, B => n106, Y => n136);
   U171 : no210 port map( A => n107, B => n105, Y => n106);
   U172 : na210 port map( A => output_type1_1_port, B => n105, Y => n135);
   U173 : iv110 port map( A => n137, Y => N210);
   U174 : no210 port map( A => n138, B => n107, Y => n137);
   U175 : na210 port map( A => n139, B => n140, Y => n107);
   U176 : na210 port map( A => n141, B => n142, Y => n140);
   U177 : mu111 port map( A => output_type1_0_port, B => output_type0_0_port, S
                           => n134, Y => n138);
   U178 : no310 port map( A => n143, B => n144, C => N183, Y => N208);
   U179 : no310 port map( A => n145, B => n146, C => n143, Y => N207);
   U180 : na210 port map( A => n147, B => n148, Y => n145);
   U181 : no210 port map( A => n149, B => n143, Y => N206);
   U182 : no210 port map( A => n148, B => n143, Y => N205);
   U183 : no210 port map( A => n110, B => n143, Y => N204);
   U184 : no210 port map( A => n109, B => n143, Y => N203);
   U185 : no210 port map( A => output_y0_0_port, B => n143, Y => N202);
   U186 : na210 port map( A => n134, B => n139, Y => n143);
   U187 : na210 port map( A => n142, B => n117, Y => n139);
   U188 : na310 port map( A => n150, B => n148, C => n151, Y => n117);
   U189 : no310 port map( A => n152, B => n144, C => n153, Y => N201);
   U190 : iv110 port map( A => n154, Y => n152);
   U191 : no210 port map( A => n146, B => n142, Y => n154);
   U192 : mu111 port map( A => n155, B => n156, S => n144, Y => N200);
   U193 : no210 port map( A => n146, B => n153, Y => n156);
   U194 : iv110 port map( A => n157, Y => n155);
   U195 : na210 port map( A => n158, B => N198, Y => n157);
   U196 : na210 port map( A => n134, B => n159, Y => N199);
   U197 : na210 port map( A => n158, B => n108, Y => n159);
   U198 : na210 port map( A => n160, B => n161, Y => n158);
   U199 : na210 port map( A => n146, B => n147, Y => n161);
   U200 : iv110 port map( A => n151, Y => n160);
   U201 : no210 port map( A => n153, B => n162, Y => N198);
   U202 : na210 port map( A => n134, B => n163, Y => N197);
   U203 : na210 port map( A => n148, B => n108, Y => n163);
   U204 : no210 port map( A => n110, B => n153, Y => N196);
   U205 : no210 port map( A => n109, B => n153, Y => N195);
   U206 : no210 port map( A => output_y0_0_port, B => n153, Y => N194);
   U207 : na210 port map( A => n134, B => n108, Y => n153);
   U208 : na210 port map( A => n142, B => n164, Y => n108);
   U209 : na210 port map( A => n150, B => n165, Y => n164);
   U210 : na210 port map( A => n147, B => n166, Y => n165);
   U211 : na210 port map( A => n162, B => n167, Y => n166);
   U212 : na210 port map( A => n168, B => n141, Y => n167);
   U213 : no310 port map( A => n169, B => n170, C => n142, Y => N193);
   U214 : na210 port map( A => n134, B => n171, Y => N192);
   U215 : na210 port map( A => n172, B => n129, Y => n171);
   U216 : na210 port map( A => n173, B => n174, Y => n172);
   U217 : na210 port map( A => n175, B => n150, Y => n174);
   U218 : iv110 port map( A => n170, Y => n173);
   U219 : no210 port map( A => n150, B => n175, Y => n170);
   U220 : no210 port map( A => n147, B => n176, Y => n175);
   U221 : mu111 port map( A => n177, B => n178, S => n147, Y => N191);
   U222 : no210 port map( A => n176, B => n169, Y => n178);
   U223 : no210 port map( A => n179, B => n148, Y => n176);
   U224 : no210 port map( A => n179, B => n180, Y => n177);
   U225 : iv110 port map( A => N189, Y => n180);
   U226 : iv110 port map( A => n149, Y => n179);
   U227 : na210 port map( A => n134, B => n181_port, Y => N190);
   U228 : na210 port map( A => n149, B => n129, Y => n181_port);
   U229 : ex210 port map( A => n162, B => n168, Y => n149);
   U230 : iv110 port map( A => n146, Y => n162);
   U231 : no210 port map( A => n169, B => n148, Y => N189);
   U232 : no210 port map( A => n110, B => n169, Y => N188);
   U233 : no210 port map( A => n109, B => n169, Y => N187);
   U234 : no210 port map( A => output_y0_0_port, B => n169, Y => N186);
   U235 : na210 port map( A => n134, B => n129, Y => n169);
   U236 : na310 port map( A => n144, B => n142, C => n182_port, Y => n129);
   U237 : no210 port map( A => n151, B => n183_port, Y => n182_port);
   U238 : no310 port map( A => n168, B => n141, C => n147, Y => n183_port);
   U239 : iv110 port map( A => n184_port, Y => n141);
   U240 : na310 port map( A => output_y0_0_port, B => n110, C => n109, Y => 
                           n184_port);
   U241 : iv110 port map( A => n185_port, Y => n109);
   U242 : ex210 port map( A => output_y0_1_port, B => output_y0_0_port, Y => 
                           n185_port);
   U243 : na210 port map( A => n186_port, B => n187_port, Y => n110);
   U244 : na210 port map( A => n188_port, B => n189_port, Y => n187_port);
   U245 : iv110 port map( A => n190_port, Y => n186_port);
   U246 : no210 port map( A => n146, B => n147, Y => n151);
   U247 : no210 port map( A => n105, B => n142, Y => N185);
   U248 : na210 port map( A => n144, B => n134, Y => N184);
   U249 : iv110 port map( A => n150, Y => n144);
   U250 : ex210 port map( A => output_y0_6_port, B => n191_port, Y => n150);
   U251 : na210 port map( A => n146, B => n134, Y => N182);
   U252 : ex210 port map( A => n192_port, B => output_y0_4_port, Y => n146);
   U253 : na210 port map( A => output_y0_3_port, B => n190_port, Y => n192_port
                           );
   U254 : na210 port map( A => n168, B => n134, Y => N181);
   U255 : iv110 port map( A => n105, Y => n134);
   U256 : no210 port map( A => n147, B => n142, Y => n105);
   U257 : ex210 port map( A => n193_port, B => output_y0_7_port, Y => n142);
   U258 : na210 port map( A => output_y0_6_port, B => n191_port, Y => n193_port
                           );
   U259 : no210 port map( A => n194_port, B => n195_port, Y => n191_port);
   U260 : iv110 port map( A => N183, Y => n147);
   U261 : ex210 port map( A => n195_port, B => n194_port, Y => N183);
   U262 : iv110 port map( A => output_y0_5_port, Y => n194_port);
   U263 : na310 port map( A => output_y0_3_port, B => n190_port, C => 
                           output_y0_4_port, Y => n195_port);
   U264 : iv110 port map( A => n148, Y => n168);
   U265 : ex210 port map( A => output_y0_3_port, B => n190_port, Y => n148);
   U266 : no210 port map( A => n189_port, B => n188_port, Y => n190_port);
   U267 : na210 port map( A => output_y0_1_port, B => output_y0_0_port, Y => 
                           n188_port);
   U268 : iv110 port map( A => output_y0_2_port, Y => n189_port);

end synthesised;



