library IEEE;
use IEEE.std_logic_1164.ALL;

entity vga_module is
 	port( 	clk, reset: in std_logic;
		x_b1: in std_logic_vector(6 downto 0); -- x van 0 tot 119 met 20 meest rechtse binnen scherm en 99 meeste linkse binne scherm
		y_b1: in std_logic_vector(7 downto 0); -- y van 0 tot 159 met 0 boven en 159 onder
		x_b2: in std_logic_vector(6 downto 0);
		y_b2: in std_logic_vector(7 downto 0);
		y_o1: in std_logic_vector(7 downto 0);
		t_o1: in std_logic_vector(2 downto 0); -- 010 is links balk 001 is rechts balk rest nog niets
		y_o2: in std_logic_vector(7 downto 0);
		t_o2: in std_logic_vector(2 downto 0);
		y_o3: in std_logic_vector(7 downto 0);
		t_o3: in std_logic_vector(2 downto 0);
		y_o4: in std_logic_vector(7 downto 0);
		t_o4: in std_logic_vector(2 downto 0);
		red,green,blue: out std_logic;
		hsync: out std_logic;
		vsync: out std_logic;
		enable: in std_logic
	);
end vga_module;

