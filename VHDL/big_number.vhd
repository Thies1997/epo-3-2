library IEEE;
use IEEE.std_logic_1164.ALL;

entity big_number is
   port(clk  :in    std_logic;
        reset:in    std_logic;
        a:in    std_logic_vector(3 downto 0);
        b  :out   std_logic_vector(3 downto 0));
end big_number;