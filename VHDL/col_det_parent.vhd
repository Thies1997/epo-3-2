library IEEE;
use IEEE.std_logic_1164.ALL;

entity col_det_parent is
   port(clk  :in    std_logic;
        reset:in    std_logic;
        x_red :in    std_logic_vector(8 downto 0);
        x_blue:in    std_logic_vector(8 downto 0);
        y_red :in    std_logic_vector(7 downto 0);
        y_blue:in    std_logic_vector(7 downto 0);
        x00   :in    std_logic_vector(8 downto 0);
        x01   :in    std_logic_vector(8 downto 0);
        x10   :in    std_logic_vector(8 downto 0);
        x11   :in    std_logic_vector(8 downto 0);
        height:in    std_logic_vector(5 downto 0);
        y0    :in    std_logic_vector(7 downto 0);
        y1    :in    std_logic_vector(7 downto 0);
        rip   :out   std_logic);
end col_det_parent;


