library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

architecture behavioural of counter is
  
  signal count, new_count : unsigned (18 downto 0);
  signal count2, new_count2 : unsigned (9 downto 0);
  signal add, new_add : unsigned (3 downto 0);
 
begin
  
P1: process (clk)
  begin
    if (rising_edge (clk)) then
      if (reset = '1' OR (count(18)='1' AND count(17)='1' AND count(16)='1'  AND count(15)='1'  AND count(14)='1'  AND count(12)='1')) then
        count <= "0000000000000000000"; 
      else
        count <= new_count;
      end if;
    end if;
end process;

P2: process (count)

  begin
      new_count <= count + add + 2;     
end process;
  
P3: process (clk)
begin
  if (rising_edge (clk)) then
    if (reset = '1' or (count2(9)='1' AND count2(8)='1' AND count2(7)='1'  AND count2(2)='1')) then
      count2 <= "0000000000";
    else
      count2 <= new_count2;
    end if;
  end if;
 
end process;
 
P4: process (count2, count)
begin
    if (count(18)='1' AND count(17)='1' AND count(16)='1'  AND count(15)='1'  AND count(14)='1'  AND count(12)='1') then
      new_count2 <= count2 + 1;
    else
      new_count2 <= count2;
    end if;
end process;
    
P5: process (clk)
begin
  if (rising_edge (clk)) then
    if (reset = '1') then
      add <= "0000";
    else
      add <= new_add;
    end if;
  end if;
end process;

P6: process (count2, add)
begin
    if (count2(9)='1' AND count2(8)='1' AND count2(7)='1'  AND count2(2)='1') then
      new_add <= add + 1;
    else
      new_add <= add;
    end if;
end process;

 count_out  <= std_logic (count(18));

 end architecture behavioural; 
