library IEEE;
use IEEE.std_logic_1164.ALL;

entity decoder is
   port(clk			:in std_logic;
	reset		:in std_logic;
	block_type		:in    std_logic_vector(2 downto 0);
	x0		:out   std_logic_vector(6 downto 0);
	x1		:out   std_logic_vector(6 downto 0);
	height		:out   std_logic_vector(5 downto 0)
);
end decoder;





