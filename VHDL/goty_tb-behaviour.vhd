library IEEE;
use IEEE.std_logic_1164.ALL;
use IEEE.numeric_std.all;

architecture behaviour of goty_tb is

component gotygolddeluxe is
	port (clk :in std_logic;
		reset: in std_logic;
		enable       :in     std_logic;
		left         :in     std_logic;
		right        :in     std_logic;
		output_y0    :out  unsigned(7 downto 0);
		output_y1    :out  unsigned(7 downto 0);
		output_y2    :out    unsigned(7 downto 0);
		output_y3    :out    unsigned(7 downto 0);
		output_type0 :out  std_logic_vector(2 downto 0);
		output_type1 :out  std_logic_vector(2 downto 0);
		output_type2 :out    std_logic_vector(2 downto 0);
		output_type3 :out    std_logic_vector(2 downto 0);
		rip          :out    std_logic	
	);
end component;

signal clk, reset, enable, left, right, rip: std_logic;
signal output_y0, output_y1, output_y2, output_y3: unsigned(7 downto 0);
signal output_type0, output_type1, output_type2, output_type3: std_logic_vector(2 downto 0);

begin
	GOTY: gotygolddeluxe port map (
		clk => clk,
		enable => enable,
		reset => reset,
		left => left,
		right => right,
		output_y0 => output_y0,
		output_y1 => output_y1,
		output_y2 => output_y2,
		output_y3 => output_y3,
		output_type0 => output_type0,
		output_type1 => output_type1,
		output_type2 => output_type2,
		output_type3 => output_type3,
		rip => rip
	);
	
	clk <= '1' after 0 ns, '0' after 81.78 ns when clk /= '0' else '1' after 81.78 ns;
	reset <= '1' after 0 ns, '0' after 20 ms, '1' after 2200 ms, '0' after 2210 ms;
	left <= '0';--, '1' after 30 ms, '0' after 560 ms, '1' after 1900 ms, '0' after 3000 ms;
	right <= '0';--, '1' after 700 ms, '0' after 1330 ms, '1' after 1500 ms, '0' after 1550 ms;
	enable <= '0';
	
end behaviour;




