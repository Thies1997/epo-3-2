library IEEE;
use IEEE.std_logic_1164.ALL;

entity rom2 is
   
  port ( address : in std_logic_vector(5 downto 0);
         q : out std_logic_vector(7 downto 0) ); 

end rom2;

