
library ieee,CellsLib;

use ieee.std_logic_1164.all;
use CellsLib.CellsLib_DECL_PACK.all;

architecture synthesised of bar_control is

   component bar
      port( counter, reset : in std_logic;  blocktype : in std_logic_vector (2 
            downto 0);  output_y0, output_y1, output_y2, output_y3 : out 
            std_logic_vector (7 downto 0);  output_type0, output_type1, 
            output_type2, output_type3 : out std_logic_vector (2 downto 0));
   end component;
   
   component counter
      port( clk, reset : in std_logic;  count_out : out std_logic);
   end component;
   
   component typebuilder
      port( clk, reset : in std_logic;  blocktype : out std_logic_vector (2 
            downto 0));
   end component;
   
   signal cnt, btype_2_port, btype_1_port, btype_0_port : std_logic;

begin
   
   C1 : typebuilder port map( clk => cnt, reset => reset, blocktype(2) => 
                           btype_2_port, blocktype(1) => btype_1_port, 
                           blocktype(0) => btype_0_port);
   C2 : counter port map( clk => clk, reset => reset, count_out => cnt);
   C3 : bar port map( counter => cnt, reset => reset, blocktype(2) => 
                           btype_2_port, blocktype(1) => btype_1_port, 
                           blocktype(0) => btype_0_port, output_y0(7) => 
                           output_y0(7), output_y0(6) => output_y0(6), 
                           output_y0(5) => output_y0(5), output_y0(4) => 
                           output_y0(4), output_y0(3) => output_y0(3), 
                           output_y0(2) => output_y0(2), output_y0(1) => 
                           output_y0(1), output_y0(0) => output_y0(0), 
                           output_y1(7) => output_y1(7), output_y1(6) => 
                           output_y1(6), output_y1(5) => output_y1(5), 
                           output_y1(4) => output_y1(4), output_y1(3) => 
                           output_y1(3), output_y1(2) => output_y1(2), 
                           output_y1(1) => output_y1(1), output_y1(0) => 
                           output_y1(0), output_y2(7) => output_y2(7), 
                           output_y2(6) => output_y2(6), output_y2(5) => 
                           output_y2(5), output_y2(4) => output_y2(4), 
                           output_y2(3) => output_y2(3), output_y2(2) => 
                           output_y2(2), output_y2(1) => output_y2(1), 
                           output_y2(0) => output_y2(0), output_y3(7) => 
                           output_y3(7), output_y3(6) => output_y3(6), 
                           output_y3(5) => output_y3(5), output_y3(4) => 
                           output_y3(4), output_y3(3) => output_y3(3), 
                           output_y3(2) => output_y3(2), output_y3(1) => 
                           output_y3(1), output_y3(0) => output_y3(0), 
                           output_type0(2) => output_type0(2), output_type0(1) 
                           => output_type0(1), output_type0(0) => 
                           output_type0(0), output_type1(2) => output_type1(2),
                           output_type1(1) => output_type1(1), output_type1(0) 
                           => output_type1(0), output_type2(2) => 
                           output_type2(2), output_type2(1) => output_type2(1),
                           output_type2(0) => output_type2(0), output_type3(2) 
                           => output_type3(2), output_type3(1) => 
                           output_type3(1), output_type3(0) => output_type3(0))
                           ;

end synthesised;



